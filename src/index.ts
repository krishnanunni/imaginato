import "reflect-metadata";
import { createConnection } from "typeorm";

createConnection().then(async connection => { }).
    catch(error => console.log(error));

const app = require('./routes/index');

console.log('Initiating Server.');
app.listen(3000, "localhost", () => {
    console.log('Server running at port 3000');
});
