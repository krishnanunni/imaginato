import { Entity, PrimaryGeneratedColumn, Column, ManyToMany, ManyToOne, CreateDateColumn, UpdateDateColumn } from "typeorm";
import { Comment } from "./Comment"

@Entity()
export class InnerComment {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    comment: string;

    @CreateDateColumn({ name: 'created_at' })
    created_at: Date;

    @UpdateDateColumn({ name: 'updated_at' })
    updated_at: Date;

    @ManyToOne(() => Comment, comment => comment.innerComments)
    commentId: Comment

}