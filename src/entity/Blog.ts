import { Entity, PrimaryGeneratedColumn, Column, OneToMany, CreateDateColumn, UpdateDateColumn } from "typeorm";
import { Comment } from "./Comment"

@Entity()
export class Blog {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    nickName: string;

    @Column()
    title: string;

    @Column()
    content: string;

    @CreateDateColumn({ name: 'created_at' })
    created_at: Date;

    @UpdateDateColumn({ name: 'updated_at' })
    updated_at: Date;

    @OneToMany(() => Comment, comment => comment.blogId)
    comments: Comment[]

}
