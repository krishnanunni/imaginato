import { Entity, PrimaryGeneratedColumn, Column, ManyToMany, ManyToOne, CreateDateColumn, UpdateDateColumn, OneToMany } from "typeorm";
import { Blog } from "./Blog"
import { InnerComment } from "./InnerComment"

@Entity()
export class Comment {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    comment: string;

    @Column()
    nickName: string;

    @CreateDateColumn({ name: 'created_at' })
    created_at: Date;

    @UpdateDateColumn({ name: 'updated_at' })
    updated_at: Date;

    @ManyToOne(() => Blog, blog => blog.comments)
    blogId: Blog

    @OneToMany(() => InnerComment, comment => comment.commentId)
    innerComments: InnerComment[]

}