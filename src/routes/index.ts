import * as http from "http";
import * as url from "url";

const blogsController = require("../controllers/blogsController");
const commentsController = require("../controllers/commentsController");
const innerCommentsController = require("../controllers/innerCommentsController");


module.exports = http.createServer((req, res) => {

    const reqUrl: any = url.parse(req.url, true);

    if (reqUrl.pathname == '/blog') {

        switch (req.method) {
            case "GET": {
                console.log('Request Type:' + req.method + ' Endpoint: ' + reqUrl.pathname);
                blogsController.getOneBlog(req, res);
                break;
            }

            case "POST": {
                console.log('Request Type:' + req.method + ' Endpoint: ' + reqUrl.pathname);
                blogsController.createBlog(req, res);
                break;
            }

            default: {
                console.log('Invalid Choice');
                break;
            }

        }

    } else if (reqUrl.pathname == '/blogs') {

        switch (req.method) {
            case "GET": {
                console.log('Request Type:' + req.method + ' Endpoint: ' + reqUrl.pathname);
                blogsController.getAllBlogs(req, res);
                break;
            }
        }
    } else if (reqUrl.pathname == '/comments') {

        switch (req.method) {
            case "GET": {
                console.log('Request Type:' + req.method + ' Endpoint: ' + reqUrl.pathname);
                commentsController.getAllComments(req, res);
                break;
            }

            case "POST": {
                console.log('Request Type:' + req.method + ' Endpoint: ' + reqUrl.pathname);
                commentsController.createComments(req, res);
                break;
            }
        }
    } else if (reqUrl.pathname == '/innerComments') {

        switch (req.method) {
            case "GET": {
                console.log('Request Type:' + req.method + ' Endpoint: ' + reqUrl.pathname);
                innerCommentsController.getAllComments(req, res);
                break;
            }

            case "POST": {
                console.log('Request Type:' + req.method + ' Endpoint: ' + reqUrl.pathname);
                innerCommentsController.createComments(req, res);
                break;
            }
        }
    }
});