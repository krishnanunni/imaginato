const url = require('url');
import { type } from "os";
import { getConnection, getRepository } from "typeorm";
import { Comment } from "../entity/Comment"

exports.getAllComments = async (req, res) => {

    try {
        const reqUrl: any = url.parse(req.url, true);

        const comments: object = await getRepository(Comment)
            .createQueryBuilder("comment")
            .leftJoinAndSelect("comment.innerComments", "innerComment")
            .where("comment.blogId = :blogId", { blogId: reqUrl.query.blogId })
            .getMany()



        res.status = 200;
        res.setHeader('Content-Type', 'application/json');
        res.end(JSON.stringify(comments));
    } catch (err) {
        res.statusCode(400).json({ error: err.message });
    }

}


exports.createComments = async (req, res) => {

    try {
        const reqUrl: any = url.parse(req.url, true);

        let data: string = ''

        await req.on('data', chunk => {
            data += chunk;
        })

        const body = JSON.parse(data)

        const comment = await getConnection()
            .createQueryBuilder()
            .insert()
            .into(Comment)
            .values([
                {
                    comment: body.comment,
                    blogId: reqUrl.query.blogId
                }
            ])
            .execute()


        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.end(JSON.stringify(comment));
    } catch (err) {
        res.statusCode(400).json({ error: err.message });
    }
}
