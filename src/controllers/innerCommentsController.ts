const url = require('url');
import { getConnection } from "typeorm";
import { InnerComment } from "../entity/InnerComment"


exports.getAllComments = async (req, res) => {

    try {
        const reqUrl: any = url.parse(req.url, true);

        const innerComments = await getConnection()
            .createQueryBuilder()
            .select("innerComment")
            .from(InnerComment, "innerComment")
            .where("innerComment.commentId = :commentId", { commentId: reqUrl.query.commentId })
            .getMany()


        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.end(JSON.stringify(innerComments));
    } catch (err) {
        res.statusCode(400).json({ error: err.message });
    }

}


exports.createComments = async (req, res) => {

    try {
        const reqUrl: any = url.parse(req.url, true);

        let data: string = ''

        await req.on('data', chunk => {
            data += chunk;
        })

        const body = JSON.parse(data)

        const innerComment = await getConnection()
            .createQueryBuilder()
            .insert()
            .into(InnerComment)
            .values([
                {
                    comment: body.comment,
                    commentId: reqUrl.query.commentId
                }
            ])
            .execute()


        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.end(JSON.stringify(innerComment));
    } catch (err) {
        res.statusCode(400).json({ error: err.message });
    }
}
