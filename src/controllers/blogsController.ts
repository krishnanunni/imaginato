const url = require('url');
import { getConnection, getRepository } from "typeorm";
import { Blog } from "../entity/Blog";

exports.getAllBlogs = async (req, res) => {

    try {
        const reqUrl: any = url.parse(req.url, true);

        const blogs: object = await getRepository(Blog)
            .createQueryBuilder("blog")
            .leftJoinAndSelect("blog.comments", "comment")
            .leftJoinAndSelect("comment.innerComments", "innerComment")
            .take(20)
            .skip(20 * (reqUrl.query.pg - 1))
            .getMany();


        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.end(JSON.stringify(blogs));
    } catch (err) {
        res.statusCode(400).json({ error: err.message });
    }
}


exports.getOneBlog = async (req, res) => {

    try {
        const reqUrl: any = url.parse(req.url, true);

        const blogRepo = await getRepository(Blog);
        const blog = await blogRepo.findOne(reqUrl.query.id, { relations: ["comments"] });

        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.end(JSON.stringify(blog));
    } catch (err) {
        res.statusCode(400).json({ error: err.message });
    }
}


exports.createBlog = async (req, res) => {

    try {
        const reqUrl: any = url.parse(req.url, true);
        let data: string = '';

        await req.on('data', chunk => {
            data += chunk;
        })

        const body = JSON.parse(data);

        const blog = await getConnection()
            .createQueryBuilder()
            .insert()
            .into(Blog)
            .values([
                {
                    nickName: body.nickName,
                    title: body.title,
                    content: body.content
                }
            ])
            .execute();


        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.end(JSON.stringify(blog));
    } catch (err) {
        res.statusCode(400).json({ error: err.message });
    }
}